package com.app.test

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.app.test.databinding.ActivityMainBinding
import com.app.test.sender.GMailSender
import android.os.StrictMode




class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        /*
        * TODO Please open below link for security to send email from background
        * https://myaccount.google.com/lesssecureapps?pli=1
        *
        * */

        val senderAddress = ""// TODO Please enter email address of sender
        val senderPassword = "" // TODO Please enter password of sender's email address


        binding.btnSend.setOnClickListener {
            if (validateFields()) {
                binding.progressBar.visibility = View.VISIBLE
                try {
                    val sender = GMailSender(senderAddress, senderPassword)
                    if (sender.sendMail(binding.edtSubject.text.toString(), binding.edtMessage.text.toString(),
                            senderAddress,binding.edtEmail.text.toString())) {
                        Toast.makeText(applicationContext, getString(R.string.success_send_email), Toast.LENGTH_LONG).show()
                        binding.progressBar.visibility = View.GONE
                    }
                } catch (e: Exception) {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(applicationContext, getString(R.string.error_send_email), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun validateFields():Boolean {
        var isValidated = false

        if (binding.edtEmail.text.isEmpty())
            showError(binding.edtEmail,getString(R.string.error_invalid_email))
         else if (binding.edtSubject.text.isEmpty())
            showError(binding.edtSubject, getString(R.string.error_invalid_subject))
        else if (binding.edtName.text.isEmpty())
            showError(binding.edtName, getString(R.string.error_invalid_name))
        else if (binding.edtMessage.text.isEmpty())
            showError(binding.edtMessage, getString(R.string.error_invalid_message))
        else
            isValidated = true

        return isValidated
    }

    private fun showError(editText: EditText, strError: String) {
        editText.error = strError
        editText.isFocusable = true
    }
}
