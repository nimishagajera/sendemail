package com.app.test.sender

import android.util.Log
import javax.activation.DataHandler

import javax.activation.DataSource

import javax.activation.FileDataSource

import javax.mail.BodyPart

import javax.mail.Message

import javax.mail.Multipart

import javax.mail.PasswordAuthentication

import javax.mail.Session

import javax.mail.Transport

import javax.mail.internet.InternetAddress

import javax.mail.internet.MimeBodyPart

import javax.mail.internet.MimeMessage

import javax.mail.internet.MimeMultipart

import java.io.ByteArrayInputStream

import java.io.IOException

import java.io.InputStream

import java.io.OutputStream

import java.security.Security

import java.util.Properties


class GMailSender(private val user: String, private val password: String) : javax.mail.Authenticator() {

    private val mailhost = "smtp.gmail.com"
    private val session: Session
    private val _multipart = MimeMultipart()

    init {

        val props = Properties()
        props.setProperty("mail.transport.protocol", "smtp")
        props.setProperty("mail.host", mailhost)
        props["mail.smtp.auth"] = "true"
        props["mail.smtp.port"] = "465"
        props["mail.smtp.socketFactory.port"] = "465"
        props["mail.smtp.socketFactory.class"] = "javax.net.ssl.SSLSocketFactory"
        props["mail.smtp.socketFactory.fallback"] = "false"
        props.setProperty("mail.smtp.quitwait", "false")
        session = Session.getDefaultInstance(props, this)
    }

    override fun getPasswordAuthentication(): PasswordAuthentication {
        return PasswordAuthentication(user, password)
    }

    @Synchronized
    @Throws(Exception::class)
    fun sendMail(subject: String, body: String, sender: String, recipients: String):Boolean {
        try {
            val message = MimeMessage(session)
            val handler = DataHandler(ByteArrayDataSource(body.toByteArray(), "text/plain"))

            message.sender = InternetAddress(sender)
            message.subject = subject
            message.dataHandler = handler

            val messageBodyPart = MimeBodyPart()
            messageBodyPart.setText(body)
            _multipart.addBodyPart(messageBodyPart)
            message.setContent(_multipart)
            if (recipients.indexOf(',') > 0)
                message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(recipients))
            else message.setRecipient(
                    Message.RecipientType.TO,
                    InternetAddress(recipients))
            Transport.send(message)
            return true

        } catch (e: Exception) {
            Log.d("Error", e.printStackTrace().toString())
            return false
        }
    }

    inner class ByteArrayDataSource(data: ByteArray, type: String) : DataSource {
        private var data: ByteArray? = data
        private var type: String? = type

        override fun getContentType(): String {
            return if (type == null)
                "application/octet-stream"
            else
                type!!
        }

        @Throws(IOException::class)
        override fun getInputStream(): InputStream {
            return ByteArrayInputStream(data)

        }

        override fun getName(): String {
            return "ByteArrayDataSource"

        }

        @Throws(IOException::class)
        override fun getOutputStream(): OutputStream {
            throw IOException("Not Supported")

        }
    }

    companion object {
        init {
            Security.addProvider(JSSEProvider())
        }
    }
}